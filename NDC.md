## Liste des objets qui devront être gérés dans la base de données:
La base de donnée est une application de gestion pour une clinique vétérinaire. L'administrateur de la clinique souhaite pouvoir gérer ses patients (animaux), ses clients, son personnel soignant ainsi que les médicaments administrés.

## Liste des propriétés associées à chaque objet:
Les clients et les personnels ont tous des noms, prénoms, date de naissance, une adresse et un numéro de téléphone, le personnel a en plus une spécialité ainsi qu’un poste.
Pour chaque animal traité on le caractérise avec un nom, une espèce, une date de naissance (qui peut être juste une année, ou inconnue), un numéro de puce d'identification (s'il en a), un numéro de passeport (s'il en a), la liste de ses propriétaires et la période durant laquelle l'animal était avec eux, ainsi que la liste des vétérinaires qui l'ont suivi et quand est-ce qu'ils l'ont fait.

Une espèce est caractérisé par un nom et une taille (petite ou moyenne).

Un dossier médical contient plusieurs entrés de différents types :une mesure de sa taille ou de son poids. Un traitement prescrit avec la date de début, la durée, le nom et la quantité à prendre par jour pour chaque médicament prescrit (on peut prescrire plusieurs molécules dans un traitement). Seul un vétérinaire peut prescrire un traitement.Des résultats d'analyses (sous forme de lien vers un document électronique). Une observation générale faite lors d'une consultation et qui l'a faite.Une procédure réalisée sur le patient avec sa description.

Une entrée enregistre la date et l’horaire de l’information entrée.

Un médicament est identifié par le nom de la molécule et est accompagnés de quelques lignes de texte décrivant leurs effets ainsi qu’une liste des espèces autorisés.

## Liste des associations

Un animal a plusieurs propriétaires qui sont les clients (d'après nôtre hypothèse). Un client peut avoir plusieurs animaux. L'association est plusieurs à plusieurs.

Un animal peut être traité par un seul vétérinaire lors de son traitement médical. Et un vétérinaire peut prendre charge de plusieurs animaux. L'association est plusieurs à un.

Une durée est associée à un client qui était avec l'animal, c'est la période durant laquelle l'animal était avec son propriétaire. L'association est un à un.

Une durée est aussi associée à un vétérinaire pour indiquer la période du suivi de l'animal. L'association est un à un.

Chaque animal appartient à un espèce. L'association est plusieurs à un.

Les médicaments sont autorisés pour certaines espèces. L'association est plusieurs à plusieurs.

Chaque personne du personnel possède se spécialise sur une ou plusieurs espèces. L'association est plusieurs à plusieurs.

Un vétérinaire préscrit plusieurs traitements. L'association est un à plusieurs.

Chaque animal a un dossier médical composé de multiples entrées. L'association est un à plusieurs.

Un traitement préscrit plusieurs utilisations des médicaments. Les utilisations des médicaments peuvent être appliquées dans plusieurs traitements. L'association est plusieurs à plusieurs. 

Plusieurs utilisations sont associées aux médicaments par rapport à l'animal traité. L'association est plusieurs à plusieurs.

Les vétérinaire font des observations. L'association est plusieurs à plusieurs.

## Liste des contraintes
Les attributs sont NOT NULL sauf qu'ils spécificent.

La date de naissance d'animal peut être sous la forme du DD/MM/YYYY ou du YYYY ou NULL.

Le numéro de la puce d'identification et le numéto de passeport d'animal peut être NULL.

Le personnel de la clinique ne doit pas être le propriétaire d'un animal traité.

La classe mesure a au moins une propriété NOT NULL, soit la taille soit le poids.

Seul un vétérinaire peut préscrire un traitement.


## Liste de héritage
<table>
    <tr>
        <th>Classe père</td>
        <th>Sous-Classe</td>
    </tr>
    <tr>
        <td>Humain</td>
        <td>Client <br> Personnel</td>
    </tr>
    <tr>
        <td>Personnel</td>
        <td>Assistant <br> Vétérinaire</td>
    </tr>
    <tr>
        <td>Entrée</td>
        <td>Mesure <br> Traitement  <br> Résultat <br> Consultation <br> Procédure</td>
    </tr>
</table>

## Liste des utilisateurs (rôles) appelés à modifier et consulter les données et leurs fonctions

<table>
    <tr>
        <th>utilisateur</th>
        <th>fonction</th>
    </tr>
    <tr>
    <td>Administateur</td>
    <td>Service informatique de la clinique vétérinaire,consulter et modifier toutes les données</td>
    </tr>
    <tr>
    <td>client</td>
    <td>Consulter et modifier ses propres données.<br>Consulter les données des vétérinaires, des assistants.<br>Consulter les dossiers médicales et les médicaments qui concernent son animal traité.</td>
    </tr>
    <tr>
    <td>Vétérinaire/Assistant</td>
    <td>Consulter et modifier ses propre données<br>Consulter et modifier les données de ses patients(animaux).<br>Consulter les données de ses clients.<br>Consulter et modifier les dossiers médicales concernant ses patients.<br>Consulter les données de médicaments.</td>
    </tr>

</table>


## Toutes les hypothèses et autres remarques utiles pour la modélisation

### Information non mentionné dans le cahier des charges qui pourrait être utile  

(1)	Enregistrer la cause et l'heure du décès des animaux décédés pendant le traitement. Ces informations sont stockées dans la classe *procédure* sous forme de description.<br>
(2)	Pour chaque animal traité, il a une facture qui enregistre le coût du traitement, y compris les frais de médicaments, les frais d'hospitalisation.<br>
(3)	Chaque médicament a un prix correspondant.<br>

### Hypothèse : 

(4) On suppose que la liste des propriétaires de chaque animal traité provient de la liste des clients.<br>




